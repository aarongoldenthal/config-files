# Changelog

## v11.3.0 (2025-02-23)

### Changed

- Updated `Linters/.golangci.yml` for v1.64.5.
  - Replaced deprecated `tenv` with `usetesting.os-setenv: true`.
  - Enabled new option `relative-path-mode: gomod`.
  - Replaced `gomodguard` with `depguard`.

## v11.2.1 (2025-02-22)

### Fixed

- Add suppression to `node-osv-scanner.toml` global config for `esbuild` CORS
  vulnerability [GHSA-67mh-4wv8-2f99](https://osv.dev/vulnerability/GHSA-67mh-4wv8-2f99).

## v11.2.0 (2025-01-09)

### Changed

- Updated `Linters/.golangci.yml` for v1.63.4. Enabled new linters `exptostd`,
  `nilnesserr`, and `usetesting` added in v1.63.0.

### Fixed

- Updated `*.gitlab-ci.yml` files to templates v37.3.1.
- Update container image to Node v22.13.0.

## v11.1.0 (2024-11-24)

### Changed

- Updated applicable `package.json` files to include a `test:no-coverage`
  script, change `main` to `exports`, and update `repository` value to
  `git+https` based on `publish` warnings. (#89)
- Updated `Linters/.golangci.yml` to enable `iface` and `recvcheck`, added in
  v1.62.0, and disabled `exportloopref`, which is not required in Go 1.22+.
  (#90)

### Fixed

- Fixed CI pipeline to update OCI image `annotations` to be correct for this
  project. Previously, the `annotations` from the base `node` image
  were cascading to this image.

## v11.0.0 (2024-08-23)

### Changed

- BREAKING: Updated all ESLint configs to flat config format for v9, renamed to
  `*.eslint.config.js`. Added `esm` config. Removed `all-configs` as
  not currently used, and less useful with flat config. (#85)
- Updated `jest.config.json` to ignore ESLint flat config. (#85)
- Added `OSV-Scanner/node-osv-scanner.toml` with common suppressions for
  all Node.js projects.
- Update all `*.renovate.json` configurations to use Renovate presets v1.1.0.

### Fixed

- Updated all `*.depcheckrc.json` files to remove unused `ignores`. (#86)

## v10.6.0 (2024-05-08)

### Changed

- Updated `.golangci.yml`:
  - Enabled `canonicalheader` and `fatcontext`, added in v1.58.0.
  - Replaced deprecated `gomnd` with `mnd`.
  - Removed deprecated `execinquery`.
  - Updated from source file, including settings for `gomodguard`,
    `perfsprint`, `sloglint`.

## v10.5.0 (2024-04-26)

### Changed

- Updated npm and lint configs to deprecate Node 21 (end-of-life on
  2024-06-01) and add support for Node 22 (`current` release as of
  2024-04-25). Compatible with all current and LTS releases
  (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#82)

## v10.4.0 (2024-03-21)

### Changed

- Updated `.golangci.yml` to enable `copyloopvar` and `intrange`, added in
  v1.57.0.

## v10.3.0 (2024-02-20)

### Changed

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#80)

## v10.2.0 (2024-02-11)

### Changed

- Updated `.golangci.yml` to enable `spancheck`, added in v1.56.0.

## v10.1.1 (2024-01-22)

### Fixed

- Updated default Vale config to disable `Google.Parens` and `Google.Passive`
  rules.

## v10.1.0 (2024-01-15)

### Changed

- Added default `.vale.ini` configuration file. Also added `.gitlab/vale` to
  `*.gitignore` files. (#78)

## v10.0.2 (2024-01-03)

### Fixed

- Updated scripts to properly substitute group placeholder with a new group
  name that has a subgroup. (#77)

## v10.0.1 (2024-01-03)

### Fixed

- Updated Dockerfile to set execute permission on all scripts. (#76)

## v10.0.0 (2024-01-02)

### Changed

- BREAKING: Moved all `create-` scripts and data files to `scripts/` folder so
  scripts can be run from any location. (#1)
  - File lists (for example, `scripts/npm-default-files.json`) now reference
    `src` from the project root for simplicity.
  - Updated `create-package.sh` script to `create-npm-package`.
- BREAKING: Added `create-container.sh` script to create a new container
  project. Updated `docker.gitlab-ci.yml` to `container.gitlab-ci.yml` for
  consistency. (#1)
- Default license for new projects changed to Apache-2.0.
- Updated `group` and `project` placeholder substitution to dynamically
  identify applicable files in `create-` script. (#75)
- Updated `SECURITY.md` with placeholder `group` and `project` names. (#75)
- Updated all license years to 2024.

## v9.2.0 (2023-11-24)

### Changed

- Updated default `.editorconfig` to use tabs for `*.go` files. (#71)
- Updated `.markdownlint.json` for breaking changes in `markdownlint@0.32.0`
  (and in `mardownlint-cli2@v0.11.0`). (#73)
- Updated `.golangci.yml` with new linters from `golangci-lint` v1.53.0 -
  1.55.0 (#70)
  - Enabled: [`gochecksumtype`](https://github.com/alecthomas/go-check-sumtype),
    [`gosmopolitan`](https://github.com/xen0n/gosmopolitan),
    [`inamedparam`](https://github.com/macabu/inamedparam),
    [`mirror`](https://github.com/butuzov/mirror),
    [`musttag`](https://github.com/go-simpler/musttag),
    [`perfsprint`](https://github.com/catenacyber/perfsprint),
    [`protogetter`](https://github.com/ghostiam/protogetter),
    [`sloglint`](https://github.com/go-simpler/sloglint), and
    [`tagalign`](https://github.com/4meepo/tagalign).
  - Disabled: [`testifylint`](https://github.com/Antonboom/testifylint).
- Updated `.yamllint.yml` with `comments:min-spaces-from-content: 1` (was 2).

### Fixed

- Updated `.gitattributes` to specify that common binary formats are not text,
  which is the default for all files. (#72)

## v9.1.0 (2023-10-24)

### Changed

- Added default `.golangci.yml` config file. (#69)
- Added default `.gitlab-ci.yml` file for Go projects. (#69)
- Added default `.devcontainer.json` file for Go projects. (#69)

## v9.0.1 (2023-09-09)

### Fixed

- Fixed `.npmpackagejsonlintrc.json` rule `prefer-scripts` for new sscript
  names.

## v9.0.0 (2023-09-04)

### Changed

- BREAKING: Updated npm and lint configs to deprecate Node 16 support since
  end-of-life as of 2023-09-11. Compatible with all current and LTS releases
  (`^18.12.0 || >=20.0.0`). (#66)
- Updated `package.json` script names to better follow convention (e.g.
  `lint:js` vs `lint-js`). Also updated associated git hooks. (#65)
- Added `typescript.package.json` with scripts to build and test types. (#68)

### Fixed

- Updated `playwright.config.js` for latest ESLint rules.
- Updated Renovate configurations to use `matchDatasources` for `docker` rules.
- Updated year in license copyrights.

## v8.0.0 (2023-07-12)

### Changed

- BREAKING: Updated all references to ignore the `Archive/` directory to
  the `archive/` directory. (#61)
- BREAKING: Removed the `GitLab/.codeclimate.json` file since no longer used.
  (#61)
- BREAKING: Updated all renovate configuration files for breaking changes in
  v36. (#59)
- BREAKING: Updated NPM config files (`*.package.json`,
  `.npmpackagejsonlintrc.json`) to drop Node 19 (since EOL 2023-06-01) and
  revert `.npmpackagelintrc.json` rule `valid-values-engines` to error after
  Node 14/19 deprecation complete. (#53, #56)
- BREAKING: Update all `package.json` `prettier` scripts for Prettier v3. (#60)
- Updated Nunjucks.djlintrc to ignore `set` blocks. (#58)
- Updated `Linters/.stylelintrc.json` to the latest configuration. (#61)

## v7.2.0 (2023-05-06)

### Changed

- Updated applicable NPM files to move from `markdownlint-cli` to
  `markdownlint-cli2`. (#57)
  - Updated `.markdownlint.json` to set `first-line-h1` to false, which matches
    the desired configuration for most projects (README, GitLab issue templates).
  - Updated all applicable files for `markdownlint-cli2` (`*.package.json`
    files `lint-md` script, `*.depcheckrc.json`, default modules for new NPM
    projcts).

## v7.1.0 (2023-04-27)

### Changed

- Change `.npmpackagelintrc.json` rule `valid-values-engines` to warning during
  Node version transition. (#55)
- Updated NPM config files (`*.package.json`, `.npmpackagejsonlintrc.json`) to
  drop Node 14 (since EOL 2023-04-30) and add Node 20 (released 2023-04-18).
  - This is not considered breaking because the `npm-package-json-lint`
    `valid-values-engines` rule is a warning (#52, #54)
- Updated `all-configs.eslintrc.json` to include `eslint-comments-config`,
  `playwright-config`, and `esm-config`. (#48)

## v7.0.0 (2023-04-04)

### Changed

- BREAKING: Moved `Linters/.eslintrc.json` to `Linters/default.eslintrc.json`
  and updated to use the `recommended` configuration. Removed
  `Linters/web.eslintrc.json` since redundant with `recommended`. Added new
  config `Linters/all-configs.eslintrc.json` with all configurations explicitly
  called out to simplify using a subset. (#43)
- BREAKING: Updated `.yamllint.yml` to add new rule `anchors` from yamllint
  v1.30.0. (#51)
- Updated default `.codeclimate.json` to disable all analyzers except
  `duplication` and `fixme`. Any other language checks are performed as
  separate linting jobs. (#50)

### Fixed

- Updated `package.json` files to latest `lint-js` script. (#45)

### Miscellaneous

- Added `lint_sh` job to CI pipeline and resolved linting issues.

## v6.0.0 (2022-11-28)

### Changed

- BREAKING: Updated default `.yamllint.yml` to explicitly state configuration
  for all rules, removing reliance on the `default` configuration. This
  included enabling some new rules, changing some settings on existing
  rules, and explicitly specifying values for all settings in all enabled
  rules. (#35)
- Updated default `.stylelintrc.json` based on changes in
  `@aarongoldenthal/stylelint-config-standard` v11.0.0. (#36)
- Added [Playwright](https://playwright.dev/) configuration to web NPM projects,
  including the required NPM packages, a default playwright config, playwright
  placeholder tests, .gitignore for playwright folders, config for eslint,
  playwright rules for renovate, and npm test script.
  - For consistency, with multiple test frameworks in the project, moved the
    Jest test template to the `Jest` folder (#41)
- Added AGPL-3.0 license. (#40)

### Fixed

- Updated `package.json` `repository` defaults to remove `git+` prefix.
- Updated `.gitlab-ci.yml` files to templates v13.0.2

## v5.3.0 (2022-08-11)

### Changed

- Add default `yamllint` config file. (#34)

## v5.2.0 (2022-07-30)

### Changed

- Added new djlint settings for 1.8 (`blank_line_before_tag`) and 1.9 (`format_css` and `format_js`). (#33)

## v5.1.0 (2022-07-29)

### Changed

- Added `.djlintrc` config file for [nunjucks](https://mozilla.github.io/nunjucks/) templates and nunjucks `package.json` template with scripts. (#31)

## v5.0.0 (2022-07-20)

### Changed

- BREAKING: Added new rules from `markdownlint` v0.32.0 to `.markdownlint.json` (`link-fragments`, `reference-links-images`, `link-image-reference-definitions`). (#30)

### Fixed

- Added missing `.htmlhintrc` to NPM web package default files. (#29)

## v4.0.1 (2022-06-20)

### Fixed

- Updated to latest `.eslintrc.json` configuration. (#26)
- Update MIT license template to 2022. (#27)
- Fixed several npm package issues: (#28)
  - Updated `lint-css` and `lint-html` scripts in `web.package.json` to eliminate errors when no files found.
  - Added placeholder `index.js` file to eliminate `lint-js` script error when no files found.
  - Added placeholder `tests/index.test.js` file to eliminate `test` script error when no files found and to meet coverage threshold.

## v4.0.0 (2022-06-17)

### Changed

- BREAKING: Updated npm and lint configs to deprecate Node 12 and 17 support since both are end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`). (#24, #20)

### Miscellaneous

- Updated to latest CI templates v8, which improve support for `main` branches, and remove unnecessary pipeline overrides. (#3)

## v3.0.0 (2022-05-12)

### Changed

- BREAKING: Moved project to <https://gitlab.com/gitlab-ci-utils/config-files>. (#25)

### Fixed

- Updated `docker.gitlab-ci.yml` to use Container Build Test Deploy template (instead of Docker) (#23)

### Miscellaneous

- Configured [`renovate`](https://github.com/renovatebot/renovate) to manage dependency updates. (#21)

## v2.1.0 (2022-05-01)

### Changed

- Updated `.npmpackagejsonlintrc.json` setting `valid-values-engines` to drop Node 12 since end-of-life and make warning duirng the transition.
- Updated git hooks for latest npm scripts. (#19)
- Added test coverage threshold requirements to `jest` configuration. (#18)

## v2.0.0 (2022-04-18)

### Changed

- BREAKING: Return `.npmpackagejsonlintrc.json` setting `prefer-scripts` to error now that projects are converted to new scripts. (#16)

### Miscellaneous

- Update pipeline to use `Container-Build-Test-Deploy` collection. (#17)

## v1.0.0 (2022-04-16)

Initial release
