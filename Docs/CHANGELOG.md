# Changelog

## Unreleased

### Added

-

### Changed

-

### Fixed

-

### Miscellaneous

-

## v1.0.0 (2021-09-20)

Initial release
