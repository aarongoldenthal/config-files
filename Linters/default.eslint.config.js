'use strict';

const recommendedConfig = require('@aarongoldenthal/eslint-config-standard/recommended');

module.exports = [
    ...recommendedConfig,
    {
        ignores: ['.vscode/**', 'archive/**', 'node_modules/**', 'coverage/**'],
        name: 'ignores'
    }
];
